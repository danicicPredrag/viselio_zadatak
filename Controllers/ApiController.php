<?php

class ApiController
{

    const API_KEY = '45137cf10e001607d23abfa01cb3069f';

    const API_ADDRESS = 'https://a2.viselio.com/';

    const GET_DATA_API_URL = 'https://r.viselio.com/';
    const GET_DATA_TOKEN = '4597065b0f5abc60b0311cf28f317229';

    private $path;
    private $method;
    private $content_type;
    private $json_data;
    private $response_status;
    private $response_message;

    public function createNewVisaApplication(\Controllers\VisaController $visa_controller)
    {
        $this->path = self::API_ADDRESS . '/' . self::API_KEY . '/public/appdata';
        $this->method = 'POST';
        $this->content_type = 'json';
        $this->json_data = json_encode([
            'email' => $visa_controller->email,
            'destination' => $visa_controller->destination_code,
            'office_id' => $visa_controller->office_id,
            'nationality' => $visa_controller->nationality_code,
            'visa_type' => $visa_controller->visa_type,
            'entries_number' => $visa_controller->entries_number,
            'expedited_service' => $visa_controller->expedited_service,
            'first_name' => $visa_controller->first_name,
            'middle_name' => $visa_controller->middle_name,
            'last_name' => $visa_controller->last_name,
            'contact_phone' => $visa_controller->contact_phone,
        ]);

        $result = $this->makeAPICall();
        if ($result == false) {
            /**
             * This is not good place to set error messages
             * but I was assuming that this method will be only called from "VisaController"
             * and I already made that clear in method parameter so ...
             */
            $visa_controller->errors['messages'][] = 'There was error in creating your visa application ';
            $visa_controller->errors['messages'][] = 'Response Status:"' . $this->response_status . '"';
            $visa_controller->errors['messages'][] = 'Response Message: "' . $this->response_message . '"';
            return;
        }
        $visa_controller->messages['messages'][] = 'Your Visa Application is created';
        $visa_controller->messages['messages'][] = 'Yur Application ID is: ' . $result->result->app_id;
    }

    public function checkIsVisaRequired($country_from, $country_to)
    {
        $this->path = self::GET_DATA_API_URL . 'api/timShort/' . self::GET_DATA_TOKEN . '/' . $country_from . '/' . $country_to;
        $this->method = 'GET';
        $result = $this->makeAPICall();
        if (isset($result->data)) {
            return $result->data;
        }
        return false;
    }

    public function getCountriesList()
    {
        $this->path = self::GET_DATA_API_URL . 'api/allNationalities/' . self::GET_DATA_TOKEN;
        $this->method = 'GET';
        $result = $this->makeAPICall();
        return $result;
    }

    private function makeAPICall()
    {

        // We have only POST and GET method in this task so lets keep it minimalistic
        if ($this->method == 'POST') {
            $curl = curl_init($this->path);
            curl_setopt($curl, CURLOPT_POST, 1);
            if ($this->content_type == 'json') {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $this->json_data);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                $result = curl_exec($curl);
                $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                $result = json_decode($result);
                if ($http_status != 200) {
                    $this->response_status = $http_status;
                    $this->response_message = $result->message;
                    return false;
                }
                return $result;
                curl_close($curl);
            }
        } else {
            return json_decode(file_get_contents($this->path));
        }

    }
}