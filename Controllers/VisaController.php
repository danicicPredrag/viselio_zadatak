<?php

namespace Controllers;

use ApiController;
use Traits\FormValidationTrait;

class VisaController
{

    use FormValidationTrait;

    public $countries_list;

    public $email;
    public $office_id;
    public $destination_id;
    public $destination_code;
    public $destination_name;
    public $nationality_id;
    public $nationality_code;
    public $nationality_name;
    public $visa_type;
    public $entries_number;
    public $expedited_service;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $contact_phone;

    public $errors = [
        'messages' => []
    ];
    public $errors_exist;
    public $messages = [
        'messages' => []
    ];

    public function __construct()
    {
        if (!empty($_POST)) {
            $this->createVisaApplication();
        }
    }

    public function getCountriesList()
    {
        $api_controller = new ApiController();
        $this->countries_list = $api_controller->getCountriesList();
    }

    public function createVisaApplication()
    {

        $this->getCountriesList();
        //validate data from form ( if fails return and display error messages that would be already set )
        if (!$this->validateCreateApplicationData()) {
            return;
        }


        // We need to get country ISO-2 codes in order to check does customer need visa for this country
        if (
            !$this->getCountryCode($this->destination_id, 'destination') ||
            !$this->getCountryCode($this->nationality_id, 'nationality')
        ) {
            return;
        }

        $api_controller = new ApiController();
        $visa_required = $api_controller->checkIsVisaRequired($this->nationality_id, $this->destination_id);

        // if visa is not required there is no need to create new visa application
        if (!$visa_required) {
            $this->errors['messages'][] = 'Can not determinate does  ' . $this->nationality_name . ' need visa to enter in ' . $this->destination_name;
            return;
        }

        if (strtolower($visa_required->visa_required) != strtolower('Yes')) {
            $this->errors['messages'][] = 'People from ' . $this->nationality_name . ' don\'t need visa to enter in ' . $this->destination_name;
            if (!empty($visa_required->note_en)) {
                $this->errors['messages'][] = 'Additional info: ' . $visa_required->note_en;
            }
            return;
        }

        $api_controller->createNewVisaApplication($this);
    }

    /**
     * @param $country_id
     * @param $destination_or_nationality : string
     * @return bool
     */
    private function getCountryCode($country_id, $destination_or_nationality)
    {
        $name = $destination_or_nationality . '_name';
        $code = $destination_or_nationality . '_code';

        foreach ($this->countries_list as $country) {
            if ($country_id == $country->id) {
                $this->$name = $country->name;
                break;
            }
        }

        foreach (_COUNTRY_LIST_ as $country) {
            if ($this->$name == $country['name']) {
                $this->$code = $country['code'];
                break;
            }
        }

        if (empty ($this->$code) || empty ($this->$name)) {
            $this->errors['messages'][] = 'we can not find ' . $destination_or_nationality . ' that you selected';
            return false;
        }

        return true;
    }
}