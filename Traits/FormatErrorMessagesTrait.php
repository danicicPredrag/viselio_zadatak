<?php

namespace Traits;

/**
 * Trait FormatErrorMessagesTrait
 * @package Traits
 */
trait FormatErrorMessagesTrait
{

    /**
     * Only purpose of this Trait is in case we want to displays messages
     * in using different logic we will have to change code on fewer places
     * @param $string
     * @return string
     */
    public function formatErrorMessages($string)
    {
        return strtoupper(str_replace('_', ' ', $string));
    }
}