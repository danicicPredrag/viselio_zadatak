<?php

namespace Traits;

use FormValidatorHelpers\RequiredValidator;
use FormValidatorHelpers\AplhabeticalStringValidator;
use FormValidatorHelpers\CountryValidator;
use FormValidatorHelpers\EmailValidator;
use FormValidatorHelpers\IntegerValidator;

trait FormValidationTrait
{

    public function validateCreateApplicationData()
    {
        $this->errors_exist = false;
        // Rules for validating form
        $rules = [
            'email' => ['required', 'email'],
            'office_id' => ['required', 'integer', 'positive'],
            'destination' => ['required', 'integer', 'country'],
            'nationality' => ['required', 'integer', 'country'],
            'visa_type' => ['required', 'integer', 'positive'],
            'entries_number' => ['required', 'integer', 'positive'],
            'expedited_service' => ['required', 'integer'],
            'first_name' => ['aplhabeticalString'],
            'last_name' => ['aplhabeticalString'],
            'middle_name' => ['aplhabeticalString'],
            'contact_phone' => ['integer'],
        ];

        $validation_passed = $this->validateFieldData($rules);
        // If validation passed fill the object witt properties from form
        // There is no need for setters and getters since data should be already validated in $this->validateFieldData();
        if ($validation_passed) {
            $this->email = $_POST['email'];
            $this->office_id = $_POST['office_id'];
            $this->destination_id = $_POST['destination'];
            $this->nationality_id = $_POST['nationality'];
            $this->visa_type = $_POST['visa_type'];
            $this->entries_number = $_POST['entries_number'];
            $this->expedited_service = $_POST['expedited_service'];
            $this->first_name = $_POST['first_name'];
            $this->middle_name = $_POST['middle_name'];
            $this->last_name = $_POST['last_name'];
            $this->contact_phone = $_POST['contact_phone'];
        }

        if (!$validation_passed) {
            $this->errors['messages'][] = 'Wrong data for Create new Visa Application';
        }
        return $validation_passed;
    }

    private function validateFieldData($rules)
    {
        // Find for each form field and for each rule: Call validation class for this particular rule
        foreach ($rules as $field_name => $single_field_rule) {
            foreach ($single_field_rule as $rule) {
                $validation_class_name = 'FormValidatorHelpers\\' . ucfirst($rule) . 'Validator';

                if (!class_exists($validation_class_name)) {
                    $this->errors_exist = true;
                    $this->errors['messages'][] = 'Internal server error inside validation';
                    return false;
                }
                $validation_class = new $validation_class_name;
                $validator_response = $validation_class->validate($field_name);
                if ($validator_response !== true) {
                    $this->errors_exist = true;
                    $this->errors['messages'][] = $validator_response;
                }
            }
        }


        if ($this->errors_exist) {
            return false;
        }
        return true;
    }
}