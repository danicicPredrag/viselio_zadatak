<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container">
        <br><br>
        <div class="row">
            <?php
            foreach ($visa_controller->errors['messages'] as $error_message) {
                echo "<div class='alert alert-danger' role='alert'>$error_message</div>";
            }

            ?>
        </div>
        <div class="row">
            <form method="post">
                <div class="form-group">
                    <label for="input_email">Email address *</label>
                    <input type="email" class="form-control" id="input_email" placeholder="Enter email" name="email" >
                </div>
                <div class="form-group">
                    <label for="office_id">Office Id *</label>
                    <select class="form-control" id="destination" name="office_id">
                        <option value="1">1</option>
                    </select>
                </div>
                <div class="form-controll">
                    <label for="destination">Destination *</label>
                    <select class="form-control" id="destination" name="destination">
                        <option value="">-</option>
                        <?php
                        foreach ($visa_controller->countries_list as $country) {
                            echo"<option value='$country->id'>$country->name</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nationality">Nationality *</label>
                    <select class="form-control" id="nationality" name="nationality">
                        <option value="">-</option>
                        <?php
                        foreach ($visa_controller->countries_list as $country) {
                            echo"<option value='$country->id'>$country->name</option>";
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="visa_type">Visa Type *</label>
                    <select class="form-control" id="nationality" name="visa_type">
                        <option value="1">1</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="entries_number">Number of Entries*</label>
                    <input type="text" class="form-control" id="entries_number" placeholder="Number of Entries" name="entries_number" required>
                </div>
                <div class="form-group">
                    <label for="expedited_service">Expedited Service *</label>
                    <select class="form-control" id="expedited_service" name="expedited_service">
                        <option value="0">0</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="first_name">First Name</label>
                    <input type="text" class="form-control" id="first_name" placeholder="First Name" name="first_name">
                </div>
                <div class="form-group">
                    <label for="middle_name">Middle Name</label>
                    <input type="text" class="form-control" id="middle_name" placeholder="Middle Name" name="middle_name">
                </div>
                <div class="form-group">
                    <label for=last_name>Last Name</label>
                    <input type="text" class="form-control" id="last_name" placeholder="Last Name" name="last_name">
                </div>
                <div class="form-group">
                    <label for="contact_phone">Contact Phone</label>
                    <input type="text" class="form-control" id="contact_phone" placeholder="Contact Phone" name="contact_phone">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</body>
</html>