<?php

namespace FormValidatorHelpers;

use Traits;

class EmailValidator implements FormValidatorInterface
{
    use Traits\FormatErrorMessagesTrait;

    /**
     * @param $field_name
     * @return bool|string
     */
    public function validate($field_name)
    {
        $data = $_POST[$field_name];

        if (empty($data) || filter_var($data, FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        $field_name_for_display = $this->formatErrorMessages($field_name);
        return $field_name_for_display . ' must be email';
    }
}