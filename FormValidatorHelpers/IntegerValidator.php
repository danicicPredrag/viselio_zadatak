<?php

namespace FormValidatorHelpers;

use Traits;

class IntegerValidator implements FormValidatorInterface
{
    use Traits\FormatErrorMessagesTrait;

    /**
     * @param $field_name
     * @return bool|string
     */
    public function validate($field_name)
    {
        $data = $_POST[$field_name];
        str_replace(' ', '', $data);
        if (empty($data) || filter_var($data, FILTER_VALIDATE_INT)) {
            return true;
        }

        $field_name_for_display = $this->formatErrorMessages($field_name);
        return $field_name_for_display . ' field can contain only numbers';

    }
}