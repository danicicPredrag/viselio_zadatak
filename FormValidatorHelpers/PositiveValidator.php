<?php

namespace FormValidatorHelpers;

use Traits;

class PositiveValidator implements FormValidatorInterface
{
    use Traits\FormatErrorMessagesTrait;

    /**
     * @param $field_name
     * @return bool|string
     */
    public function validate($field_name)
    {

        $data = $_POST[$field_name];
        if (ctype_alpha($data) || $data > 0) {
            return true;
        }

        $field_name_for_display = $this->formatErrorMessages($field_name);
        return $field_name_for_display . ' be greater than 0';
    }
}