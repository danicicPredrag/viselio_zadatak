<?php

namespace FormValidatorHelpers;

use Traits;

class AplhabeticalStringValidator implements FormValidatorInterface
{
    use Traits\FormatErrorMessagesTrait;

    /**
     * @param $field_name
     * @return bool|string
     */
    public function validate($field_name)
    {

        $data = $_POST[$field_name];
        // someone can have name that consist of 2 words
        $data = str_replace(' ', '', $data);
        if (empty($data) || ctype_alpha($data)) {
            return true;
        }

        $field_name_for_display = $this->formatErrorMessages($field_name);
        return $field_name_for_display . ' must contain only letters';
    }
}