<?php

namespace FormValidatorHelpers;

use Traits;

class RequiredValidator implements FormValidatorInterface
{
    use Traits\FormatErrorMessagesTrait;

    /**
     * @param $field_name
     * @return bool|string
     */
    public function validate($field_name)
    {
        if (!empty($_POST[$field_name]) ||
            $_POST[$field_name] == 0) {
            return true;
        }

        $field_name_for_display = $this->formatErrorMessages($field_name);
        return $field_name_for_display . ' field is required';
    }
}