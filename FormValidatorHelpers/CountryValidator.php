<?php

namespace FormValidatorHelpers;

use Traits;

class CountryValidator implements FormValidatorInterface
{
    use Traits\FormatErrorMessagesTrait;

    /**
     * @param $field_name
     * @return bool|string
     */
    public function validate($field_name)
    {
        $data = $_POST[$field_name];
        if (!filter_var($data, FILTER_VALIDATE_INT) ||
            $data < 1 ||
            $data > 245
        ) {
            $field_name_for_display = $this->formatErrorMessages($field_name);
            return $field_name_for_display . ' Imposible country';
        }

        return true;
    }
}