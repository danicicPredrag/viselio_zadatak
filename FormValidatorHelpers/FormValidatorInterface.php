<?php

namespace FormValidatorHelpers;

interface FormValidatorInterface
{
    public function validate($field_name);
}