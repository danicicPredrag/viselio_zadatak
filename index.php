<?php
session_start();
require_once('constants/country_list.php');
require_once('vendor/autoload.php');

use Controllers\VisaController;

$visa_controller = new VisaController();
$visa_controller->getCountriesList();

if (empty($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = bin2hex(random_bytes(32));
}
$token = $_SESSION['csrf_token'];

include(__DIR__ .'/views/createVisaForm.php');